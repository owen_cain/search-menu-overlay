# Features:

### UI

- nav bar 
  - Arrows to go back/forward by asat date. (far left).
  - A search button to activate the modal content. (done)
  - Link to report source code.
  - Link to devops story.  
- modal
    - Add filters along with search.
    - Activate search with double shift. Close with enter. (done)
    - Use arrows and enter to navigate.
  
- Landing page with welcome message and tips on how to use the site

