

function openModal() {
  modalIsOpen = true;
  modal.style.display = "block";
  mySearch.focus();
  mySearch.select();
 }

function closeModal() {
    modal.style.display = "none";
    modalIsOpen = false;
 }

 // Function is called in the search input html element
 function searchFilter() {
  var input, filter, ul, li, a, i;
  input = document.getElementById("mySearch");
  if (input.value.length - old_input_length != 0) {
    filter = input.value.toUpperCase();
    ul = document.getElementById("myMenu");
    li = ul.getElementsByTagName("li");
    isSelectedUpdated = false;
    for (i = 0; i < li.length; i++) {
      a = li[i].getElementsByTagName("a")[0];
      if (a.innerHTML.toUpperCase().indexOf(filter) > -1) {
        li[i].classList.remove('hideItem');
        li[i].classList.add('showItem');
        if (isSelectedUpdated == false) {
          li[i].classList.add('selected')
          isSelectedUpdated = true
        }
        else {
          li[i].classList.remove('selected')
          }
      } else {
        li[i].classList.remove('showItem');
        li[i].classList.add('hideItem');
      }
    }
  }
  old_input_length = input.value.length
}

function selectedDropdown(select) {
    var selectedValue = $(`#${select} option:selected`).text();
    if (select == "year") {
        // initialise the month dropdown values
        year = selectedValue
        populateMonthDropdown()
        }
    if (select == "month") {
        // initialise the day dropdown values
        month = selectedValue
        populateDayDropdown()
        }

    populateList();
    return selectedValue
    }

// creates the dropdown element for id
function createDropdownElement(id, set_value) {
    if (id == 'year') {check = year};
    if (id == 'month') {check = month};
    if (id == 'day') {check = day};
    if (id == 'status') {check = status};
    if (id == 'product') {check = product};
    if (id == 'instance') {check = instance};
    if (id == 'entity') {check = entity};


    var selectedAttrib = document.createAttribute("selected");
    var option = document.createElement("option");
    var value = document.createAttribute("value");
    value.value = set_value;
    option.setAttributeNode(value);
    if (check == set_value) {option.setAttributeNode(selectedAttrib)};
    var node = document.createTextNode(set_value);
    option.appendChild(node);
    document.getElementById(id).appendChild(option);
    }

function showDateSelect() {
    document.getElementById("year").style.display = "";
    document.getElementById("month").style.display = "";
    document.getElementById("day").style.display = "";
    }

function hideDateSelect() {
    document.getElementById("year").style.display = "none";
    document.getElementById("month").style.display = "none";
    document.getElementById("day").style.display = "none";
}

function toggleShowDateSelect() {
    if (document.getElementById('year').style.display != "none"){
        hideDateSelect();
        }
    else{
        showDateSelect();
        }
    historical = !historical;
    mySearch.focus();
    populateList();
    }

function populateMonthDropdown() {
    $('#month option[value!="hide"]').remove()
    if (year == currentYear) {
        for (i=1; i < currentMonth + 2; i++) {
            createDropdownElement('month', i)
            }
        }
    else {
        for (i=1; i < 13; i++) {
            createDropdownElement('month', i)
            }
        }
    populateDayDropdown()
    }

function populateDayDropdown() {
    $('#day option[value!="hide"]').remove()
    if (year == currentYear & (month) == (currentMonth + 1)) {
        for (i=1; i < currentDay + 2; i++) {
            createDropdownElement('day', i)
            }
        }
    else {
        numberOfDays = new Date(year, month, 0).getDate()
        for (i=1; i < numberOfDays; i++) {
            createDropdownElement('day', i)
            }
        }
    }

function getDropdownValues(id){
    $.getJSON("reports_json/dropdown_filters.json", function(result){
        $.each(result[id], function(i, field){
            createDropdownElement(id, field)
        });
      });
    }

function getReportJson(){
    // get report details
    return $.get('reports_json/2021/1/report_details.json');
    }

function createListItem(year, month, day, status, product, instance, entity, name){
    nodeText = `${year}-${month}-${day} ${product}${instance}${entity} ${name}`
    var node = document.createTextNode(nodeText);
    input = document.getElementById("mySearch");
        var li = document.createElement("li");
        var a = document.createElement("a");
        var i = document.createElement("i");
        var i_class = document.createAttribute("class");
        var i_style = document.createAttribute("style");
        var li_onclick = document.createAttribute("onclick");
        var data_content = document.createAttribute("data-content");
        var reportPath = `reports/${name}/${status}/${year}/${month}/${day}/${product}/${instance}/${entity}/index.html`;
        data_content.value = reportPath;
        i_class.value = "fa fa-circle";
        i_style.value = "color:"+status;
        li_onclick.value = `loadPage("${reportPath}")`;
        i.setAttributeNode(i_class);
        i.setAttributeNode(i_style);
        li.setAttributeNode(li_onclick);
        li.setAttributeNode(data_content);

        a.appendChild(i);
        a.appendChild(node);
        li.appendChild(a);

    if (nodeText.toUpperCase().indexOf(input.value.toUpperCase())>-1) {
        li.classList.add('showItem');

        if (selectedInitialised == false) {
            li.classList.add('selected');
            selectedInitialised = true
            };
        }
    else {
        li.classList.add('hideItem');
    }
        document.getElementById("myMenu").appendChild(li);

}

function populateList(){
    $('#myMenu li[value!="hide"]').remove()
    if (historical) {
        var json_path = `reports_json/${year}/${month}/report_details.json`;
        }
    else {
        var json_path = `reports_json/current/report_details.json`;
        };
    $('ul li').removeClass('selected');
    selectedInitialised = false;
    allReports = $.get(json_path, function(data) {
      $.each( data, function( key, val ) {
        if ((val.year == year | year == "year" | !historical) &
            (val.month == month | month == "month" | !historical) &
            (val.day == day | day == "day" | !historical) &
            (val.status == status | status == "status") &
            (val.product == product | product == "product") &
            (val.instance == instance | instance == "instance") &
            (val.entity == entity | entity == "entity")){
            createListItem(val.year, val.month, val.day, val.status, val.product, val.instance, val.entity, val.name);
            }
      });
    });
    mySearch.focus();
}

function getUrlVariables() {
    const urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('year')) {year=urlParams.get('year');}
    if (urlParams.has('month')) {month=urlParams.get('month');}
    if (urlParams.has('day')) {day=urlParams.get('day');}
    if (urlParams.has('status')) {status=urlParams.get('status');}
    if (urlParams.has('product')) {product=urlParams.get('product');}
    if (urlParams.has('instance')) {instance=urlParams.get('instance');}
    if (urlParams.has('entity')) {entity=urlParams.get('entity');}
    if (urlParams.has('searchInput')) {searchInput=urlParams.get('searchInput');}
    if (urlParams.has('reportPath')) {reportPath=urlParams.get('reportPath');}
    if (urlParams.has('historical')) {historical=urlParams.get('historical')=="true";}
    if (urlParams.has('modalIsOpen')) {modalIsOpen=urlParams.get('modalIsOpen')=="true";}
};

function setUrlVariables() {
    queryString = `?&year=${year}&month=${month}&day=${day}&status=${status}&product=${product}&instance=${instance}&entity=${entity}&historical=${historical}&modalIsOpen=${modalIsOpen}&reportPath=${reportPath}`;
    window.history.pushState({path:queryString},'',queryString)
};

function updateReportNavHistory(path) {
    reportHistoryNumber = reportHistoryNumber + 1;
    reportNavHistory = reportNavHistory.slice(0, reportHistoryNumber);
    reportNavHistory.push(path);
    setArrowOpacity();
};

function setArrowOpacity() {
    if (leftArrowClickable()) {document.getElementById('leftArrowButton').style.opacity = 1}
    else {document.getElementById('leftArrowButton').style.opacity = 0.2};
    if (rightArrowClickable()) {document.getElementById('rightArrowButton').style.opacity = 1}
    else {document.getElementById('rightArrowButton').style.opacity = 0.2};
}

function leftArrowClickable() {
    return (reportHistoryNumber > 0) & (reportHistoryNumber <= reportNavHistory.length);
    };

function rightArrowClickable() {
    return (reportHistoryNumber >= 0) & (reportHistoryNumber < (reportNavHistory.length - 1));
    };

function leftArrowButton() {
    if (leftArrowClickable()) {
        reportHistoryNumber = reportHistoryNumber - 1;
        reportPath = reportNavHistory[reportHistoryNumber];
        $('#content').load(reportPath);
        setUrlVariables();
    }
    setArrowOpacity();
};

function rightArrowButton() {
    if (rightArrowClickable()) {
        reportHistoryNumber = reportHistoryNumber + 1;
        reportPath = reportNavHistory[reportHistoryNumber];
        $('#content').load(reportPath);
        setUrlVariables()
        }
    setArrowOpacity();
};

function loadPage(path) {
    updateReportNavHistory(path);
    reportPath=path;
    $('#content').load(path);
    input = document.getElementById("mySearch");
    setUrlVariables();
    closeModal();
    }

function populateThemeSelect() {
    var theme_select = document.getElementById('theme-selector')
    var option = document.createElement("option");
    var value = document.createAttribute("value");
    value.value = 'hide'
    option.setAttributeNode(value);
    option.appendChild(document.createTextNode('Theme'));
    theme_select.appendChild(option);
    for (i=0; i<themeNameList.length; i++) {
        var option = document.createElement("option");
        var value = document.createAttribute("value");
        value.value = themeNameList[i];
        option.setAttributeNode(value);
        option.appendChild(document.createTextNode(themeNameList[i]));
        theme_select.appendChild(option);
        }
    }

function setTheme(selectedTheme) {
    var body = document.getElementById('nav-page-body')
    for (i=0; i<themeNameList.length; i++) {
        body.classList.remove(themeNameList[i])
        }
    body.classList.add(selectedTheme)
    localStorage.setItem('theme', selectedTheme)
    }

previousKeyCode = null;
modalIsOpen = false;
old_input_length = 0;
selectedInitialised = false;
theme = localStorage.getItem('theme');
if (theme == null) {theme = 'darcula';};

themeNameList = ['darcula', 'moonlight', 'light', 'solarized-light', 'solarized-dark', 'material-oceanic']
var ul = document.getElementById("myMenu");
var modal = document.getElementById("myModal");
var mySearch = document.getElementById("mySearch");
var span = document.getElementsByClassName("close")[0];
var openModalButton = document.getElementById("openModalButton");
var startYear = 2018;
var startMonth = 3;
var currentYear = new Date().getFullYear();
var currentMonth = new Date().getMonth();
var currentDay = new Date().getDate();
var theme_select = document.getElementById('theme-selector')

reportNavHistory = []
reportHistoryNumber = 0;
reportPath = "reports/home.html";
searchInput = "";
year=currentYear
month=currentMonth+1;
day="day";
status="status";
product="product";
instance="instance";
entity="entity";
historical=false;

setArrowOpacity();
populateThemeSelect();
setTheme(theme);
// initialise year, month and day as being hidden
document.getElementById("year").style.display = "none";
document.getElementById("month").style.display = "none";
document.getElementById("day").style.display = "none";

// get the values from the url and init state
getUrlVariables()
// initialise the select list
populateList()
if (historical) {
    showDateSelect();
    checkbox = window.document.getElementById('recentcheckbox')
    checkbox.checked=true;
};

if (reportPath) {
    reportHistoryNumber = -1;
    loadPage(reportPath);
    console.log(reportPath)};
// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    closeModal();
  }
}
if (searchInput != "") {document.getElementById("mySearch").value = searchInput};

// initialise the year , month, day dropdown values
for (i=startYear; i < currentYear + 1; i++) {
    createDropdownElement('year', i)
}
populateMonthDropdown();
populateDayDropdown();

// initialise the non-date dropdown values
getDropdownValues('status')
getDropdownValues('entity')
getDropdownValues('product')
getDropdownValues('instance')

// click on the search button to open the modal
openModalButton.onclick = function() {
  openModal();
}

// logic that defines keys activating events
document.addEventListener('keydown', function(event) {
    // double shift
    if (event.keyCode == 16 & previousKeyCode == 16) {
        openModal();
    }
    // enter
    if (event.keyCode == 13) {
        closeModal();
    }

    ul = document.getElementById("myMenu");
    li = ul.getElementsByClassName("showItem");

    // down with modal open
    if (modalIsOpen & (event.keyCode == 38 | event.keyCode == 40)) {
        changedSelectedItem = false;
        for (i = 0; i < li.length; i++) {
            if (li[i].classList.contains("selected")) {
                if (event.keyCode == 40) {
                    if (i != li.length-1 & changedSelectedItem == false) {
                        li[i].classList.remove("selected");
                        li[i+1].classList.add("selected");
                        li[i+1].scrollIntoView(false);
                        changedSelectedItem = true;
                        }
                    }
                else if (event.keyCode == 38) {
                    if (i != 0) {
                        li[i].classList.remove("selected");
                        li[i-1].classList.add("selected");
                        li[i-1].scrollIntoView(false);
                        }
                    }
                }
            }
        }
    if (event.keyCode == 13) {
        for (i = 0; i < li.length; i++) {
            if (li[i].classList.contains("selected")) {
                loadPage(li[i].getAttribute('data-content'));
            }
        }

    }

    previousKeyCode = event.keyCode
}, true);



theme_select.addEventListener('change', event => {
    setTheme(theme_select[theme_select.selectedIndex].value)
    })

document.getElementById('year').addEventListener('change', event => {
    year=selectedDropdown('year');
    })

document.getElementById('month').addEventListener('change', event => {
    month=selectedDropdown('month');
    })

document.getElementById('day').addEventListener('change', event => {
    day=selectedDropdown('day');
    })

document.getElementById('status').addEventListener('change', event => {
    status=selectedDropdown('status');
    })

document.getElementById('product').addEventListener('change', event => {
    year=selectedDropdown('product');
    })

document.getElementById('instance').addEventListener('change', event => {
    instance=selectedDropdown('instance');
    })

document.getElementById('entity').addEventListener('change', event => {
    entity=selectedDropdown('entity');
    })

