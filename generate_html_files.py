import os
import json

report_locations = {"year": [2018, 2019, 2020, 2021],
                    "month": range(12),
                    "day": range(5),
                    "name": [f"name{i}" for i in range(10)],
                    "status": ["red", "orange", "green"],
                    "entity": ["A"],
                    "product": ["A"],
                    "instance": ["A"]}
dropdown_filters = {k: v for (k, v) in report_locations.items() if k not in ["year", "month", "day"]}
print(dropdown_filters)
path = "reports_json"
if not os.path.exists(path):
    os.makedirs(path)
file = open(os.path.join(path, "dropdown_filters.json"), "w")
file.write(json.dumps(dropdown_filters))
file.close()
for year in report_locations["year"]:
    for month in report_locations["month"]:
        print(year, month)
        month_list = []
        path = os.path.join("reports_json", str(year), str(month))
        if not os.path.exists(path):
            os.makedirs(path)
        f = open(os.path.join(path, "report_details.json"), "w")
        for day in report_locations["day"]:
            for name in report_locations["name"]:
                for status in report_locations["status"]:
                    for entity in report_locations["entity"]:
                        for product in report_locations["product"]:
                            for instance in report_locations["instance"]:
                                path = os.path.join('reports', str(name), str(status), str(year), str(month), str(day),
                                                              str(product), str(instance), str(entity))
                                if not os.path.exists(path):
                                    os.makedirs(path)
                                html_file = open(os.path.join(path,  'index.html'), 'w')
                                html_file.write(f"<h1 style='color:{status};'>{name}{status}{year}{month}{day}{product}{instance}{entity}<h1>")
                                html_file.close()
                                month_list.append({"year": year,
                                                   "month": month,
                                                   "day": day,
                                                   "name": name,
                                                   "status": status,
                                                   "entity": entity,
                                                   "product": product,
                                                   "instance": instance})
            if year == 2021 and month == 2 and day == 4:
                if not os.path.exists("reports_json/current/"):
                    os.makedirs("reports_json/current/")
                file2 = open("reports_json/current/report_details.json", "w")
                file2.write(json.dumps([i for i in month_list if
                                        i['year'] == 2021 and i['month'] == 2 and i['day'] == 4]))
                file2.close()
        f.write(json.dumps(month_list))
        f.close()

